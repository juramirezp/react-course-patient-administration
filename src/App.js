import React, {Fragment, useState, useEffect} from 'react';
import Form from './components/Form'
import Date from './components/Date'

function App() {

  // Citas en LocalStorage
  let datesToStart = JSON.parse(localStorage.getItem('dates'));
  if(!datesToStart){
    datesToStart = [];
  }

  // Arreglo que almacena ciats
  const [dates, setDates] = useState(datesToStart);

  // Funcion que se dispara cuando el state cambia
  useEffect(() => {
    if(datesToStart){
      localStorage.setItem('dates', JSON.stringify(dates));
    }else{
      localStorage.setItem('dates', JSON.stringify([]));
    }
    
    // Se indica que states debe escuchar
  }, [dates] );

  // Funcion que agrega nueva cita
  const newDate = date => {
    setDates([
      ...dates,
      date
    ])
  }

  // Funcion que elimina cita por id
  const deleteDate = id => {
    const updatedDates = dates.filter(date => date.id !== id);
    setDates(updatedDates);
  }

  // Mensaje condicional
  const title = dates.length === 0 ? 'No existen citas registradas' : 'Administra tus Citas';

  return (
    <Fragment>
      <h1>Administrador de Pacientes</h1>

      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Form
              newDate={newDate}
            ></Form>
          </div>
          <div className="one-half column">
            <h2>{title}</h2>
            {dates.map(date => (
              <Date
                key={date.id}
                date={date}
                deleteDate={deleteDate}
              />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
